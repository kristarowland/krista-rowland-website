function enablePage(page)
{
  $(".page").hide();
  $("#page-" + page).show();

  if (page === "title") {
     $("nav").hide();
  }
  else {
    $("nav").show();
  }
}

function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

$(document).ready(function()
{
  $('.image').hover(
    function()
    {
      $(this).animate({
        opacity: 0.5,
      })
    },
    function()
    {
      $(this).animate({
        opacity: 1,
      })
    }
  );

  $("[data-fancybox]").fancybox({
      buttons : [
        'fullScreen',
        'close'
      ]});
});


$(document).ready(function()
{
  enablePage("title");

  $("a.link").click(e => 
  {
    let href = e.target.attributes.href.value;
    enablePage(href.substring(1));
  });

  $(document).mousemove(function(e)
  {
    let mouseX = e.pageX;
    let mouseY = e.pageY;

    let ww = $(window).width();
    let wh = $(window).height();

    let traX = 100 * (mouseX / ww);
    let traY = 100 - (100 * (mouseY / wh));

    traX = 25 + traX / 2;
    traY = 25 + traY / 2;

    $(".title-main").css({"background-position": traX + "%" + traY + "%"});
  });
});

